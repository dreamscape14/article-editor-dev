<?php

use Dreamscape\Foundation\ACL;
use Dreamscape\Foundation\ChromeDebug;

require __DIR__.'/../vendor/autoload.php';

/*
 * Shared DB bind
 */

app()->bind('db', new \Dreamscape\Database\DatabaseContracted(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_NAME));

/**
 * Twig Settigns
 */
$twig_settings = [];
//if (! environment::is_development()) {
//    $twig_settings['cache'] = 'templates/_twig/_cache';
//}
$twig_loader = new \Twig\Loader\FilesystemLoader('templates/_twig');
$twig = new Twig\Environment($twig_loader, $twig_settings);

/**
 * Twig extensions
 */
$twig_ext_funcs = [
    'attribute_if_in' => static function ($needle, $haystack, $attribute) {
        if (! is_array($haystack)) {
            $haystack = [$haystack];
        }
        return in_array($needle, $haystack, false) ? $attribute : '';
    },
    'preview_url' => static function ($article_id) {
        return Dreamscape\UseCase\PreviewForArticle\reseller_url($article_id);
    }
];

$twig_ext_filters = [
    'only_allowed' => static function ($value, array $allowed) {
        return in_array($value, $allowed, false);
    },
];

foreach ($twig_ext_funcs as $name => $closure) {
    $twig->addFunction(new \Twig\TwigFunction($name, $closure));
}
foreach ($twig_ext_filters as $name => $closure) {
    $twig->addFilter(new \Twig\TwigFilter($name, $closure));
}

$twig->addGlobal('permissions', ACL::roles());
$twig->addGlobal('TWIG_PREVIEW_LOCATION', PREVIEW_LOCATION);
$twig->addGlobal('TWIG_REQUEST_URI', $_SERVER['REQUEST_URI']);
$twig->addGlobal('CRAZY_HOME', CRAZY_HOME);

$twig->addExtension(new Twig_Extensions_Extension_Text());

/**
 * Twig Container
 */
app()->bind('twig', $twig);

