alter table generic_status add column order_key int;

-- select * from generic_status gs where lower(gs.status_name) = 'published';
update generic_status gs set gs.order_key = 1 where lower(gs.status_name) = 'published';
-- select * from generic_status gs where lower(gs.status_name) = 'unpublished';
update generic_status gs set gs.order_key = 2 where lower(gs.status_name) = 'unpublished';
-- select * from generic_status gs where lower(gs.status_name) = 'hold';
update generic_status gs set gs.order_key = 3 where lower(gs.status_name) = 'hold';
-- select * from generic_status gs where lower(gs.status_name) = 'finished';
update generic_status gs set gs.order_key = 4 where lower(gs.status_name) = 'finished';

