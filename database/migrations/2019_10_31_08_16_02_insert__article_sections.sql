-- new Categories for Crazy/Aust
insert into article_sections (section_title, meta_description, meta_keywords, priority) values
    ('Office 365', 'Office 365 Description', 'Office 365 keywords', 1000),
    ('Open Xchange', 'Open Xchange Description', 'Open Xchange keywords', 1001),
    ('SEO', 'SEO Description', 'SEO keywords', 1002),
    ('Managing Your Blog', 'Managing Your Blog Description', 'Managing Your Blog Keywords', 1003),
    ('Kelola Blog', 'Kelola Blog', 'Kelola Blog', 1004);

-- New Categories for Vodien
insert into article_sections (section_title, section_description, meta_keywords, priority) values
    ('Pre-Signup', 'Vodien, Pre-Signup', 'vodien signup pre-signup', 1005),
    ('Account Manager', 'Vodien, Account Manager', 'vodien account manager', 1006),
    ('Billing', 'Vodien, Billing', 'vodien billing', 1007),
    ('Domain Names', 'Vodien, Domain Names', 'vodien domain names', 1008),
    ('Email Hosting', 'Vodien, Email Hosting', 'vodien ', 1009),
    ('Web Hosting', 'Vodien, Web Hosting', 'vodien web hosting', 1010),
    ('FTP', 'Vodien, FTP', 'vodien ftp', 1011),
    ('Wordpress Hosting', 'Vodien, Wordpress Hosting', 'vodien wordpress hosting', 1012),
    ('CMS', 'Vodien, CMS', 'vodien cms', 1014),
    ('SSL', 'Vodien, SSL', 'vodien ssl', 1015),
    ('SEO', 'Vodien, SEO', 'vodien seo', 1016),
    ('VPS', 'Vodien, VPS', 'vodien vps', 1017),
    ('Dedicated Server', 'Vodien, Dedicated Server', 'vodien dedicated server', 1018),
    ('Secure Shell', 'Vodien, Secure Shell', 'vodien ssh secure shell', 1019),
    ('Affiliate', 'Vodien, Affiliate', 'vodien affiliate', 1020),
    ('Reseller', 'Vodien, Reseller', 'vodien reseller', 1021),
    ('Policy', 'Vodien, Policy', 'vodien policy', 1022),
    ('Useful Tips', 'Vodien, Useful Tipes', 'vodien tips', 1023);

-- Rename "Search Engine Ads" -> "Online Marketing Hub"
update article_sections s set s.section_title = 'Online Marketing Hub' where s.section_id = 10;


