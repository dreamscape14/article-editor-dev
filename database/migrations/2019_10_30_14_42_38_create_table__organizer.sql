create table organizer (
    id int auto_increment not null primary key ,
    section_id int not null,
    section_type varchar(64) not null,
    created_at timestamp default current_timestamp
);

