<?php

error_reporting(E_ALL);

require_once __DIR__ . '/bootstrap/app_legacy.php';

require_once __DIR__ . '/bootstrap/app.php';

/*
 * Full Steam Ahead!
 */

set_exception_handler(static function (Exception $e) {
    $err_message = '[Article Editor Dev]: ' . $e->getMessage();
    $trace_data = $e->getTraceAsString();
    bugtracker::generic_error($err_message, bugtracker::ERROR_TYPE_GENERAL, $trace_data);
});

router::init();
router::start();

restore_exception_handler();

