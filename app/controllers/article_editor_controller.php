<?php

use Dreamscape\Foundation\ACL;
use Dreamscape\Http\JsonResponse as DsJsonResponse;
use Dreamscape\Model\Article;
use Dreamscape\Repository\ArticleCirlcesRepository;
use Dreamscape\Repository\ArticleRepository;
use Dreamscape\Repository\ArticleStatusRepository;
use Dreamscape\Repository\CommentRepository;
use Dreamscape\Repository\CommentStatusRepository;
use Dreamscape\Repository\Enum\DocTypeEnum;
use Dreamscape\Repository\Enum\LocaleEnum;
use Dreamscape\Repository\Enum\ResellerEnum;
use Dreamscape\Repository\OrganizerMutableRepository;
use Dreamscape\Repository\Repository;
use Dreamscape\Repository\ResellerRepository;
use Dreamscape\Repository\SectionMutableRepository;
use Dreamscape\Repository\SectionRepository;
use Dreamscape\Validators\ArticleValidator;
use Dreamscape\Repository\Enum\PaginatorPresetsEnum;
use Dreamscape\UseCase\PreviewForArticle;
use Dreamscape\UseCase\BrokenLinksMonitor;

class article_editor_controller {

    const SEARCH_RESULTS = 'Search Results';

    /**
	 * @var template - Sigma template object
	 */
	private $_template;

	/**
	 * @var article_editor Model object
	 */
	private $article_editor,
			$article_content_issues;

	private $moderator_member_id = array(
		'production' => 0,
		'development' => 7165901,
	);

	private $moderator_crms_user_id = array(
		'production' => 0,
		'development' => 3187,
	);

	private $message_preview_length = 15;

	/**
	 * Initializing private classes
	 */
	public function __construct() {
	    if (! ACL::ensureAny(ACL::$ARTICLE_TOOL_ROLES)) {
            display_and_die('403');
        }

		$this->_template = new template();
		$this->article_editor = new article_editor();
		$this->article_content_issues = new help_section\article_content_issues();
	}

    public function index()
    {
        $circlesRepository = new ArticleCirlcesRepository();
        $circles = $circlesRepository->all();

        $articles = new ArticleRepository();
        $recently_inserted = $articles->recenltyInserted(Repository::$RECENTLY_QUERY_LIMIT);
        $recently_updated = $articles->recenltyUpdated(Repository::$RECENTLY_QUERY_LIMIT);

        $sections = (new SectionRepository())->organized();

        display('index',
            compact('sections', 'circles', 'recently_inserted', 'recently_updated')
        );
    }

    public function article_list()
    {
        $query_section_id = input::get_id('section_id');
        $filters = input::expose(ArticleRepository::$FILTERS);
        $currentPage = input::get_id('page', 1);
        $perPage = input::get_id('perPage', 0);

        $article_repo = (new ArticleRepository());

        $paginator_presets = PaginatorPresetsEnum::$VALUES;
        $paginator = (new Dreamscape\Paginator\Paginator(
            $article_repo->filterBy($filters, $page = 0), $perPage)
        )->paginate($currentPage);

        $articles = $article_repo->filterBy($filters, $currentPage, $perPage);

        $section_repo = (new SectionRepository());
        $sections = $section_repo->organized();
        $section_info = $section_repo->cached__by_id($query_section_id);
        $section_title = sprintf('%s (%s)', $section_info['section_title'], $section_info['section_total']);

        display('article_list',
            compact('section_title', 'sections', 'articles', 'query_section_id',
                'paginator', 'paginator_presets', 'perPage')
        );
    }

    public function organizer()
    {
        if (! ACL::ensurePermissions(['ARTICLE_TOOL_PUBLISHER_ROLE'])) {
            display_and_die('403');
        }

        $sections = (new SectionRepository())->organized();
        $available_brands = (new ResellerRepository)->notOrganizedYet();
        display('organizer', compact('sections', 'available_brands'));
    }

    public function organizer_save()
    {
        if (! ACL::ensurePermissions(['ARTICLE_TOOL_PUBLISHER_ROLE'])) {
            display_and_die('403');
        }

        $data = input::getJson();

        $synced = (new SectionMutableRepository())->sync($data);
        $response = (new OrganizerMutableRepository())->truncateAndInsert($synced);

        json_response(compact('response'));
    }

    public function edit()
    {

        if (! ACL::ensureAny(['ARTICLE_TOOL_EDITOR_ROLE', 'ARTICLE_TOOL_PUBLISHER_ROLE'])) {
            display_and_die('403');
        }

        $article_id = input::get_id('article_id');
        $default_section_id = input::get_id('section_id');

        $sections = (new SectionRepository())->getAll();
        $statuses = (new ArticleStatusRepository())->accordingToPermission();

        $doc_types = DocTypeEnum::all();
        $locales = LocaleEnum::all();
        $resellers = ResellerEnum::forArticleEdit();

        $article_check = ArticleValidator::check($article_id);
        $article = (new ArticleRepository())->findOrNew($article_id);
        if ($default_section_id > 0) {
            $article['section_id'] = $default_section_id;
        }

        display('editor',
            compact('article', 'article_check', 'doc_types', 'sections',
                'statuses', 'locales', 'resellers')
        );
    }

    public function preview() {
        if (! $article_id = Article::filterId(input::get('article_id'))) {
            throw new \InvalidArgumentException('Article id not provided');
        }

        $preview_location = PreviewForArticle\legacy_location($article_id);
        $preview_location_reseller = PreviewForArticle\reseller_url($article_id);

        redirect($preview_location_reseller . '/?debug=1');
    }

    public function broken_links()
    {
        $total = BrokenLinksMonitor\init();
        $environment = \environment::get();
        display('broken_links', compact('total', 'environment'));
    }

    public function broken_links_touch()
    {
        $response = DsJsonResponse::ok();
        $response->setData('untouched', -1);

        try {
            BrokenLinksMonitor\touch();
            $response->setData('untouched', BrokenLinksMonitor\count_to_touch());
        } catch (\Exception $e) {
            $response->error($e->getCode(), $e->getMessage());
        }

        echo json_response($response);
    }

    public function broken_links_result()
    {
        $articles = BrokenLinksMonitor\items_with_issues();
        $no_items_found_message = 'No issues found';
        echo view('component/article_table_index', compact('articles', 'no_items_found_message'));
    }

    /* todo: Impose MutableRepository */

	public function save_article() {
		echo json_encode($this->article_editor->save_article(input::get_array('post')));
	}

	public function update_article() {
		echo json_encode($this->article_editor->update_article(input::get_array('post')));
	}

	public function upload_files() {
		echo json_encode($this->article_editor->upload_images(input::get_files()));
	}

	public function check_doc_type() {
		echo json_encode($this->article_editor->check_doc_type(input::get_array('post')));
	}

	public function remove_files() {
		$article_id = input::get('article_id');
		$image_id = input::get('image_id');
		$error = true;

		if ($image_id) {
			if ($this->article_editor->delete_image($image_id, $article_id)) {
				$error = false;
			}
		}

		if ($error) {
			$data = array('error' => 'There was an error deleting your files!');
		} else {
			$data = array('success' => 'Images was successfully deleted!');
		}

		echo json_encode($data);
	}

	public function update_status() {
	    $data = input::getJson();
	    if (! array_key_exists('values', $data) || ! array_key_exists('status', $data)) {
			echo json_encode(0);
			return;
		}

		echo json_encode(($this->article_editor->update_status($data['values'], $data['status']) ? 1 : 0));
	}

	public function get_image() {
		$image_id = input::get('id');
		if ($image_id && $image = $this->article_editor->get_image($image_id)) {
			header('Content-type: ' . $image['mime_type']);
			echo $image['image'];
			exit;
		}

		header('Location: /');
	}

    public function article_comments()
    {

        $filters = input::expose(CommentRepository::$FILTERS);
        $currentPage = valdef(input::get('page'), 1);

        $repo = new CommentRepository();
        $paginator = (new Dreamscape\Paginator\Paginator(
            $repo->filterBy($filters, $page = 0))
        )->paginate($currentPage);
        $comments = $repo->filterBy($filters, $currentPage);

        $comment_statuses = (new CommentStatusRepository())->all();
        $token = ACL::generateCrmsLoginToken();

        $title_description = ' | ' . valdef(input::get('status_name'), 'All');

        display('article_comment', compact('title_description', 'comments', 'comment_statuses', 'paginator', 'token'));
    }

    /**
     * @param $section_name
     * @return string|string[]|null
     */
    private function resolveSectionName($section_name)
    {
        $replace = self::SEARCH_RESULTS;
        if (empty($section_name)) {
            $replace = preg_replace('/-/', ' ', $section_name);
        }
        return $replace;
    }


}

