<?php


namespace Dreamscape\Database;


final class SQLResponse
{
    public $code;

    public $message;

    public function __construct($code = 0, $message = 'No errors')
    {
        $this->code = $code;
        $this->message = $message;
    }

    public function setException(Exception $e)
    {
        $this->code = $e->getCode();
        $this->message = $e->getMessage();
    }
}

