<?php

use Dreamscape\Container\Container;

if (! function_exists('app')) {
    function app($abstract = null)
    {
        if (is_null($abstract)) {
            return Container::getInstance();
        }

        return Container::getInstance()->make($abstract);
    }
}

if (! function_exists('array_pluck')) {
    function array_pluck($items, $key) {
        return array_map(static function($item) use ($key) {
            return is_object($item) ? $item->$key : $item[$key];
        }, $items);
    }
}

if (! function_exists('array_flatten')) {
    function array_flatten(array $items) {
        $result = [];
        foreach ($items as $item) {
            if (!is_array($item)) {
                $result[] = $item;
            } else {
                array_push($result, array_values($item));
            }
        }

        return $result;
    }
}

if (! function_exists('array_only')) {
    function array_only($array, $keys)
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }
}

if (! function_exists('collect_only')) {
    function collect_only($collection, $keys)
    {
        return array_map(static function ($row) use ($keys) {
            return array_only($row, $keys);
        }, $collection);
    }
}

if (! function_exists('array_key_by_key')) {
    function array_key_by_key($array, $key, $value) {
        return array_search($value, array_column($array, $key), false);
    }
}

if (! function_exists('array_by_key')) {
    function array_by_key($array, $key, $value) {
        if (($found_key = array_key_by_key($array, $key, $value)) !== false) {
            return $array[$found_key];
        }
        return $found_key;
    }
}

if (! function_exists('array_delete_by_key')) {
    function array_delete_by_key($array, $key, $value) {
        $found_key = array_search($value, array_column($array, $key), false);
        if ($found_key !== false) {
            unset($array[$found_key]);
        }
        return $found_key;
    }
}

if (! function_exists('parenthesised')) {
    function parenthesised($str) {
        return '(' . $str . ')';
    }
}

if (! function_exists('view')) {
    function view($template, array $data) {
        return app('twig')->render($template.'.twig', $data);
    }
}

if (! function_exists('display')) {
    function display($template, array $data = []) {
        echo view($template, $data);
    }
}

if (! function_exists('display_and_die')) {
    function display_and_die($template, array $data = []) {
        display($template, $data);
        die('Sorry...');
    }
}

if (! function_exists('string_studly')) {
    function string_studly($str) {
        $studly = ucwords(str_replace(['-', '_'], ' ', $str));
        return str_replace(' ', '', $studly);
    }
}

if (! function_exists('string_camel')) {
    function string_camel($str) {
        return lcfisrt(srting_studly($str));
    }
}

if (! function_exists('valdef')) {
    function valdef($value, $default) {
        if (empty($value)) {
            $value = $default;
        }
        return $value;
    }
}

if (! function_exists('json_response')) {
    function json_response($data) {
        if (!is_object($data) && !is_array(reset($data))) {
            $data = [$data];
        }
        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }
}


