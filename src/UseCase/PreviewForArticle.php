<?php


namespace Dreamscape\UseCase\PreviewForArticle {

    use Dreamscape\Repository\ArticleRepository;
    use Dreamscape\Repository\Enum\PreviewLocationEnum;
    use Dreamscape\Repository\OrganizerRepository;

    function reseller_url($article_id) {
        $article = (new ArticleRepository())->findOrNew($article_id);
        $reseller_id = (new OrganizerRepository())->resellerIdFor($article['section_id']);
        return PreviewLocationEnum::elect($reseller_id, $article['excluded_resellers']);
    }

    function legacy_location($article_id) {
        $article = (new ArticleRepository())->articleId($article_id);
        return PREVIEW_LOCATION . $article['article_url'];
    }
}




