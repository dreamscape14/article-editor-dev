<?php

namespace Dreamscape\UseCase\BrokenLinksMonitor {

    use help_section\article_content_issues;

    const BATCH_COUNT = 131;
    const LOG_TABLE = 'sYra_help.article_comments';
    const HAS_ISSUES_STATUS = 4; // generic_status.hold

    function init() {
        app('db')->truncate(LOG_TABLE)->execute();
        return count_to_touch();
    }

    function count_to_touch() {
        $query = '
            select count(*) as total
            from sYra_help.article a
            where a.status_id <> 2 and a.article_id not in (select ac.article_id from sYra_help.article_comments ac);
        ';
        return app('db')->query($query)->fetchColumn();
    }

    function items_to_touch() {
        $batch_count = BATCH_COUNT;
        $query = "
            select
                a.article_id, a.article_title as message 
            from sYra_help.article a
            where a.status_id <> 2 and a.article_id not in (
                select ac.article_id from sYra_help.article_comments ac
                )
            order by a.date_updated desc
            limit {$batch_count};
        ";
        return app('db')->query($query)->fetchAll();
    }

    function touch($count = BATCH_COUNT) {
        $items = items_to_touch();
        $check_service = (new article_content_issues());
        $log = array_map(static function ($item) use ($check_service) {
            $item['date_added'] = date('Y-m-d H:i:s');
            $check_result = $check_service->check_current_article_for_issues($item['article_id']);
            if (! $check_result) {
                $item['message'] = $check_result;
                $item['status_id'] = HAS_ISSUES_STATUS;
            }
            mark_touched($item);
            return $item;
        }, $items);
        return $log;
    }

    function mark_touched($item) {
        return app('db')->insert($item, LOG_TABLE);
    }

    function items_with_issues() {
        $query = '
            select 
                   a.article_id, a.article_title, a.article_url,
                   a.date_updated, a.date_published, a.date_scanned,
                   a.status_id, gs.status_name as status, gs.status_color
            from sYra_help.article_comments ac
                inner join sYra_help.article a using (article_id)
                left join sYra_help.generic_status gs on (a.status_id = gs.status_id)
            where
                ac.status_id = :status_id 
        ';
        return app('db')->query($query, [':status_id' => HAS_ISSUES_STATUS])->fetchAll();
    }

    function result() {
        return [];
    }
}