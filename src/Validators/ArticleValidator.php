<?php


namespace Dreamscape\Validators;


use Dreamscape\Contracts\Validators\EntityValidator;
use help_section\article_content_issues;

class ArticleValidator implements EntityValidator
{

    public static function check($entity_id)
    {
        return (new article_content_issues())->check_current_article_for_issues($entity_id);
    }
}

