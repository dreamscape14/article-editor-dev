<?php


namespace Dreamscape\Repository;


class ResellerRepository extends Repository
{
    public function notOrganizedYet()
    {
        $query = "	
            select rs.reseller_id, rs.reseller_name
            from sYra.reseller rs
            where rs.reseller_id in (1, 900, 1023, 1344)
                and rs.reseller_id not in (
                    select distinct org.section_id from sYra_help.organizer org where org.section_type = 'brand'
        )";

        return $this->db()->query($query)->fetchAll();
    }
}

