<?php


namespace Dreamscape\Repository;


final class SectionRepository extends Repository
{
    use WithArticleStatuses;

    private function queryAll()
    {
        $query = "	SELECT  arts.section_id, 
	                        arts.section_title, 
                            concat(arts.section_title, ifnull(concat(': ', substring_index(arts.section_description, ',', 1)), '')) collate utf8_unicode_ci as section_title_full,
	                        arts.section_description,
							arts.section_icon, arts.priority, article_total.total
					FROM article_sections arts
						LEFT JOIN (
							SELECT a.section_id, COUNT(a.article_id) total
							FROM article a
							WHERE a.status_id != 2 
							GROUP BY a.section_id
						) article_total USING (section_id)
					ORDER BY arts.priority ASC";
        return $query;
    }

    private function queryAllAsNew()
    {
        return "	SELECT  
	                    1 as not_organized,
	                    arts.priority as order_key,
	                    arts.section_id, 
	                    'section' as section_type,
                        arts.section_title, 
	                    arts.section_icon,
                        ifnull(article_total.total, 0) as section_total,
	                    'n/a' as reseller_name
					FROM article_sections arts
						LEFT JOIN (
							SELECT a.section_id, COUNT(a.article_id) total
							FROM article a
							WHERE a.status_id != 2 
							GROUP BY a.section_id
						) article_total USING (section_id)
	                where arts.status = 3 
					ORDER BY arts.priority";
    }

    private function queryOrganized()
    {
        return '
            select
                org.order_key,
                org.section_id,
                org.section_type,
                if(org.section_type = \'brand\', r.reseller_name, s.section_title) as section_title,
                s.section_icon,
                ifnull(s.total, 0) as section_total,
                r.reseller_name
            from organizer org
                left join (
                    SELECT  arts.section_id, arts.section_title, arts.section_description,
                        arts.section_icon, arts.priority, article_total.total
                    FROM article_sections arts
                        LEFT JOIN (
                            SELECT a.section_id, COUNT(a.article_id) total
                            FROM article a
                            WHERE a.status_id != 2
                            GROUP BY a.section_id
                        ) article_total USING (section_id)
                    where arts.status = 3
                ) s on (s.section_id = org.section_id)
                left join sYra.reseller r on (r.reseller_id = org.section_id)
            order by order_key
        ';
    }

    public function queryCanBeDeleted($section_id)
    {
        $section_id = $this->imposeIntId($section_id);
        return "
            select count(a.article_id) as article_cnt
            from article a
            where a.section_id = {$section_id};";
    }

    public function getAll()
    {
        return $this->fetchAll($this->queryAll());
    }

    public function organized()
    {
        $sections = $this->fetchAll($this->queryOrganized());
        if (empty($sections)) {
            $sections = $this->fetchAll($this->queryAllAsNew());
        }
        return $sections;
    }

    public function canBeDeleted($section_id)
    {
        $article_cnt = (int) $this->fetchColumn($this->queryCanBeDeleted($section_id));
        return $article_cnt === 0;
    }

    public function cached__by_id($id)
    {
        return array_by_key($this->cashed, 'section_id', $id);
    }
}

