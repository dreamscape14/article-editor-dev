<?php


namespace Dreamscape\Repository;


final class OrganizerRepository extends Repository
{
    private function queryResellerId()
    {
        return '
            select o.section_id as reseller_id
            from sYra_help.organizer o
            where o.section_type = \'brand\' and o.order_key < (
                select o1.order_key from sYra_help.organizer o1 where o1.section_id = :section_id
                )
            order by o.order_key desc
            limit 1';
    }

    public function resellerIdFor($section_id)
    {
        $this->imposeIntId($section_id);
        $result = $this->db()->query($this->queryResellerId(), [':section_id' => $section_id])->fetch();

        return $result['reseller_id'];
    }
}

