<?php


namespace Dreamscape\Repository;


final class OrganizerMutableRepository extends MutableRepository
{
    protected $table = 'sYra_help.organizer';

    public function truncateAndinsert(array $values)
    {
        $values = array_filter($values, static function ($item) {
            if (isset($item['action'])) {
                return $item['action'] !== 'delete';
            }
            return true;
        });
        $values = collect_only($values, ['section_id', 'section_type', 'order_key']);
        $queries = [
            $this->compileTruncate(),
            $this->compileInsert($values)
        ];

        return (array) $this->process($queries);
    }
}

