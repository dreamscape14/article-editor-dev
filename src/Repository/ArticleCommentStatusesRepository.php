<?php


namespace Dreamscape\Repository\Schutz;


use Dreamscape\Repository\Repository;

class ArticleCommentStatusesRepository extends Repository
{
    public function all()
    {
        $query = '	SELECT status_name, status_id
					FROM article_comments_statuses';

        return $this->db()->query($query)->fetchAll();
    }
}

