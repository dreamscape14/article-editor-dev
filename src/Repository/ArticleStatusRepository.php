<?php


namespace Dreamscape\Repository;


use Dreamscape\Foundation\ACL;

final class ArticleStatusRepository extends Repository
{
    private function queryAll()
    {
        $query = 'select lcase(status_name) as status_name, status_id, status_color from generic_status';
        return $query;
    }

    public function getAll()
    {
        return $this->fetchAll($this->queryAll());
    }

    public function accordingToPermission()
    {
        $user_roles = ACL::roles();
        $statuses = $this->getAll();
        if (! $user_roles['ARTICLE_TOOL_PUBLISHER_ROLE']) {
            foreach ([3, 5] as $status_id_for_publisher) {
                if ($key = array_key_by_key($statuses, 'status_id', $status_id_for_publisher)) {
                    $statuses[$key]['disabled'] = true;
                }
            }
        }
        return $statuses;
    }
}

