<?php
namespace Dreamscape\Repository;

/**
 * @PhpVersionCheck:5.4
 */

use Dreamscape\Contracts\Database\Database as DatabaseContract;
use Dreamscape\Contracts\Database\QueryFilter as QueryFilterContract;
use Dreamscape\Database\QueryFilter;
use PDO;

abstract class Repository
{
    /* @var DatabaseContract */
    private $db;

    protected $filterClassPostfix = 'FilterClass';

    protected $defaultQuery = 'queryAll';

    protected $cashed = [];

    protected $disable_global_filters = false;

    public static $RECENTLY_QUERY_LIMIT = 5;

    public static $FILTERS_NAMESPACE = 'Dreamscape\Repository\Filters';

    public function __construct(DatabaseContract $db = null)
    {
        $db = $db ?: app('db');
        $this->db = $db;
    }

    public function db()
    {
        return $this->db;
    }

    protected function globalFilters()
    {
        return [];
    }

    protected function imposeIntId($id)
    {
        $id = (int) $id;
        if ($id === 0) {
            $repo = __CLASS__;
            throw new \InvalidArgumentException("[{$repo}] Invalid id provided");
        }
        return $id;
    }

    protected function disableGlobalFilters()
    {
        $this->disable_global_filters = true;
    }

    protected function enableGlobalFilters()
    {
        $this->disable_global_filters = false;
    }

    protected function applyFilters($query, array $filters)
    {
        $where = [];
        $order_by = [];

        if (! $this->disable_global_filters) {
            foreach ($this->globalFilters() as $globalFilter) {
                array_unshift($filters, $globalFilter);
            }
        }
        $filters = array_flatten($filters);

        foreach ($filters as $filter) {
            if ($filter instanceof QueryFilter) {
                if ($str = $filter->where()) {
                    $where[] = parenthesised($str);
                }
                if ($str = $filter->orderBy()) {
                    $order_by[] = $str;
                }
            }
        }

        $where_clause =  implode(' and ', $where);
        $order_by_clause = implode(', ', $order_by);

        if (! empty($where_clause)) {
            $query = "{$query} \n where {$where_clause}";
        }
        if (! empty($order_by_clause)) {
            $query = "{$query} \n order by {$order_by_clause}";
        }

        return $query;
    }

    protected function applyLimit($query, $limit = 0)
    {
        if ($limit > 0) {
            return "{$query} limit $limit";
        }

        return $query;
    }

    protected function applyOffset($query, $offset)
    {
        if ($offset > 0) {
            return "{$query} offset {$offset}";
        }

        return $query;
    }

    protected function queryPrepareForPage(&$query, $filters = [], $page, $per_page = 15)
    {
        $offset = ($page - 1) * $per_page;
        $limit = $per_page;

        $this->queryPrepare($query, $filters, $limit, $offset);
    }

    protected function queryPrepare(&$query, array $filters = [], $limit = 0, $offset = 0)
    {
        $query = trim($query);
        $query = $this->applyFilters($query, $filters);
        $query = $this->applyLimit($query, $limit);
        $query = $this->applyOffset($query, $offset);
    }

    protected function fetchAll($query, array $filters = [], $limit = 0, $offset = 0, $fetch_style = PDO::FETCH_ASSOC)
    {
        $this->queryPrepare($query, $filters, $limit, $offset);
        $this->cashed = $this->db()->query($query)->fetchAll($fetch_style);
        return $this->cashed;
    }

    protected function forPage($query, array $filters = [], $page = 1, $per_page = 15, $fetch_style = PDO::FETCH_ASSOC)
    {
        $this->queryPrepareForPage($query, $filters, $page, $per_page);
        return $this->db()->query($query)->fetchAll($fetch_style);
    }

    protected function fetch($query, array $filters = [])
    {
        $this->queryPrepare($query, $filters);
        return $this->db()->query($query)->fetch();
    }

    protected function toFilterClass($filter_name)
    {
        $filter_class = string_studly($filter_name) . $this->filterClassPostfix;
        return static::$FILTERS_NAMESPACE . '\\' . $filter_class;
    }
    private function resolveQueryFilters(array $filters)
    {
        $result = [];

        foreach (array_keys($filters) as $filter_name) {
            $filter_class = $this->toFilterClass($filter_name);
            if (class_exists($filter_class)) {
                $filter_param = $filters[$filter_name];
                $result[] = (new $filter_class($filter_param));
            } else {
                throw new \InvalidArgumentException("Filter $filter_class not found");
            }
        }

        return $result;
    }

    public function resolveQueryScope(array $query_filters)
    {
        $result = $this->defaultQuery;
        $scope_method = 'queryScope';

        foreach ($query_filters as $instance) {
            if ($instance instanceof QueryFilterContract && method_exists($instance, $scope_method)) {
                $result = $instance->$scope_method();
            }
        }

        return $result;
    }

    public function filterBy(array $filters, $page = 1, $per_page = 15)
    {
        $query_filters = $this->resolveQueryFilters($filters);
        $queryScopeMethod = $this->resolveQueryScope($query_filters);

        if ($page === 0) {
            return $this->fetchAll($this->$queryScopeMethod(), $query_filters);
        }

        return $this->forPage($this->$queryScopeMethod(), $query_filters, $page, $per_page);
    }
}

