<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Repository\Enum;


final class PaginatorPresetsEnum
{
    public static $VALUES = [
        '15' => '15',
        '50' => '50',
        '100' => '100',
        'All' => '0',
    ];

    public function all()
    {
        return self::$VALUES;
    }
}

