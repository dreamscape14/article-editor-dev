<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Repository\Enum;


final class LocaleEnum
{
    public static $VALUES = ['ae', 'au', 'in', 'nz', 'uk', 'us', 'hk', 'id', 'my', 'ph', 'sg'];

    public static function all()
    {
        return self::$VALUES;
    }
}

