<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Repository\Enum;


final class PreviewLocationEnum
{
    public static $VALUES = [
        \environment::DEVELOPMENT => [
            1       => ['system' => 'aust', 'help' => '/help/'],
            1344    => ['system' => 'crazy', 'help' => '/help/'],
            1023    => ['system' => 'cart.sitebeat', 'help' => '/members/help/'],
            900     => ['system' => 'vodien', 'help' => '/articles/'],
        ],
        \environment::PRODUCTION => [
            1       => 'https://www.austdomains.com.au/help/',
            1344    => 'https://crazydomains.com.au/help/',
            1023    => 'https://manage.sitebeat.com/members/help/',
            900     => 'https://help.vodien.com/articles/',
        ],
    ];

    public static function elect($reseller_id, $exclude_resellers = [], $member_id = null)
    {
        $env = \environment::get();

        if (! array_key_exists($env, self::$VALUES)) {
            return null;
        }

        if (! array_key_exists($reseller_id, self::$VALUES[$env])) {
            return null;
        }

        /* DSP18-14514 Algorithm */
        if ( ((int) $reseller_id === 1344) && in_array($reseller_id, $exclude_resellers, $strict = false) ) {
            $reseller_id = 1;
        }
        /* DSP18-14514 Algorithm */

        /* Host choice */
        $url = self::$VALUES[$env][$reseller_id];

        if (\environment::is_development()) {
            $http_host = $_SERVER['HTTP_HOST'];
            $dev_http_host = str_replace(key($url), current($url), $http_host);
            $url = 'http://' . $dev_http_host . $url['help'];
        }
        /* Host choice */

        return $url;
    } 
}

