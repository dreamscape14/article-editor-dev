<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Repository\Enum;


final class ResellerEnum
{
    public static $VALUES = [
        1 => 'AustDomains',
        1344 => 'CrazyDomains',
        1023 => 'Sitebeat',
        900 => 'Vodien',
    ];

    public static function forArticleEdit()
    {
//        return array_only(self::$VALUES, [1, 1344, 1023]);
        return self::$VALUES;
    }

    public static function forOrganizer()
    {
        return self::$VALUES;
    }
}

