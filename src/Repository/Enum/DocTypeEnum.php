<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Repository\Enum;


final class DocTypeEnum
{
    public static $VALUES = ['article', 'guide', 'tutorial'];

    public static function all()
    {
        $result = [];
        foreach (self::$VALUES as $value) {
            $result[] = [
                'title' => $value,
            ];
        }
        return $result;
    }

}

