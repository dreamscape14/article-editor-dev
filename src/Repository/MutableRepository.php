<?php


namespace Dreamscape\Repository;


use Dreamscape\Contracts\Database\Database as DatabaseContract;
use Dreamscape\Database\SQLResponse;

class MutableRepository
{
    private $db;

    protected $table;

    public function __construct(DatabaseContract $db = null)
    {
        $db = $db ?: app('db');
        $this->db = $db;
    }

    public function isString($value)
    {
        return is_string($value);
    }

    public function quoteString($value)
    {
        return "'{$value}'";
    }

    public function castToNumeric($value)
    {
        if (is_float($value)) {
            return (float) $value;
        }
        return (int) $value;
    }

    public function parameter($value)
    {
        return $this->isString($value) ? $this->quoteString($value) : $this->castToNumeric($value);
    }

    public function parameterize(array $values)
    {
        return implode(', ', array_map([$this, 'parameter'], $values));
    }

    public function columnize(array $columns)
    {
        return implode(', ', $columns);
    }

    public function compileTruncate()
    {
        return "truncate table {$this->table}";
    }

    public function compileInsert(array $values)
    {
        if (! is_array(reset($values))) {
            $values = [$values];
        }

        $columns = $this->columnize(array_keys(reset($values)));

        $parameters = implode(', ', array_map(function ($record) {
            return parenthesised($this->parameterize($record));
        }, $values));

        return "insert into {$this->table} ($columns) values $parameters";
    }

    public function process(array $queries)
    {
        $response = (new SQLResponse());
        
        $this->db->begin();
        try {
            foreach ($queries as $query) {
                $this->db->query($query);
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            $response->setException($e);
        }

        return (array) $response;
    }

    public function lastInsertId($query)
    {
        $this->db->query($query);
        return  $this->db->last_insert_id();
    }

    public function update($dml)
    {
        $this->db->query($dml);
        return $this->db->affected_rows();
    }

    public function delete($dml)
    {
        return $this->update($dml);
    }

    public function db()
    {
        return $this->db;
    }
}

