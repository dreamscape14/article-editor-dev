<?php


namespace Dreamscape\Repository\Filters;


use Dreamscape\Contracts\Database\QueryFilter as QueryFilterContract;

final class StatusIdCommentFilter extends CommentFilter implements QueryFilterContract
{

    private $status_id;

    public function __construct($status_id)
    {
        $this->status_id = (int) $status_id;
    }

    public function queryScope()
    {
        return 'queryList';
    }

    public function where()
    {
        return $this->where_clause($this->aliased('status_id'), '=', $this->status_id);
    }

    public function orderBy()
    {
        return '';
    }
}

