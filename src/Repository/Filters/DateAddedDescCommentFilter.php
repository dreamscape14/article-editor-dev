<?php


namespace Dreamscape\Repository\Filters;


use Dreamscape\Contracts\Database\QueryFilter as QueryFilterContract;

final class DateAddedDescCommentFilter extends CommentFilter implements QueryFilterContract
{

    public function queryScope()
    {
        return 'queryList';
    }

    public function where()
    {
        return '';
    }

    public function orderBy()
    {
        return $this->order_clause($this->aliased('date_added'), 'desc');
    }
}

