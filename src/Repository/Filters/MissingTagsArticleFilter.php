<?php


namespace Dreamscape\Repository\Filters;


use Dreamscape\Contracts\Database\QueryFilter as QueryFilterContract;
use Dreamscape\Database\QueryFilter;

class MissingTagsArticleFilter extends QueryFilter implements QueryFilterContract
{

    public function queryScope()
    {
        return 'queryFull';
    }

    public function where()
    {
        return $this->where_clause('tta.article_id IS NULL');
    }

    public function orderBy()
    {
        return 'article.article_id DESC';
    }
}

