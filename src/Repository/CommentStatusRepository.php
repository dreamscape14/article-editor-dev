<?php


namespace Dreamscape\Repository;



class CommentStatusRepository extends Repository
{
    public function all()
    {
        $query = 'select acs.status_id, acs.status_name, acs.status_color from article_comments_statuses acs';
        return $this->db()->query($query)->fetchAll();
    }
}

