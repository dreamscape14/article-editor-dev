<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Repository;


use Dreamscape\Repository\Filters\DateAddedDescCommentFilter;

final class CommentRepository extends Repository
{
    public static $FILTERS = ['status_id'];

    protected $filterClassPostfix = 'CommentFilter';

    protected $defaultQuery = 'queryList';

    protected function globalFilters()
    {
        return [
            new DateAddedDescCommentFilter(),
        ];
    }

    public function queryList()
    {
        return 'select 	ac.comment_id, ac.comment_id, ac.article_id, ac.message, ac.date_added, ac.date_published,
							IFNULL(acac.display_name, aca.display_name) display_name,
							IFNULL(acac.profile_image_url, aca.profile_image_url) profile_image_url,
							acs.status_id, acs.status_name, acs.status_color,
							a.article_url, a.article_title
					from article_comments ac
						inner join article_comments_author aca on ac.author_id = aca.author_id
						inner join article_comments_statuses acs on ac.status_id = acs.status_id
						inner join article a on ac.article_id = a.article_id
						left join sYra_help.article_comments_author_customisation acac on acac.author_id = aca.author_id';
    }
}

