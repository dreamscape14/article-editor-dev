<?php


namespace Dreamscape\Repository;


use Dreamscape\Database\SQLResponse;
use Dreamscape\Foundation\ACL;

final class SectionMutableRepository extends MutableRepository
{
    const SECTION_DELETED = 2;

    protected $table = 'sYra_help.article_sections';

    public function sync(array $values)
    {
        $response = new SQLResponse();

        $this->db()->begin();
        try {
            foreach ($values as &$row) {
                $action = $this->get_sync_action($row['action']);
                $this->$action($row);
            }
            $this->db()->commit();
        } catch (\PDOException $e) {
            $this->db()->rollback();
            $response->setException($e);
            return $response;
        }

        return $values;
    }

    private function get_sync_action($action)
    {
        return 'sync_' . $action;
    }

    private function sync_(array $row)
    {
        return false;
    }

    private function sync_insert(array &$row)
    {
        $value = array_only($row, ['section_title', 'reseller_tags', 'locale_tags', 'language_tags', 'status']);
        $value['updated_by_user'] = ACL::current_user_id();
        $insert_stmt = $this->compileInsert($value);
        $row['section_id'] = $this->lastInsertId($insert_stmt);
        return $row['section_id'];
    }

    private function sync_update(array $row)
    {
        $update_stmt = $this->compileUpdate($row);
        return $this->update($update_stmt);
    }

    private function sync_delete(array $row)
    {
        $delete_stmt = $this->compileDelete($row['section_id']);
        return $this->update($delete_stmt);
    }

    private function compileUpdate(array $row)
    {
        return sprintf(
            "update %s set section_title = '%s', reseller_tags = '%s' where section_id = %d",
            $this->table, $row['section_title'], $row['reseller_tags'], $row['section_id']
        );
    }

    private function compileDelete($section_id)
    {
        return sprintf(
            'update %s set status = %d where section_id = %d',
            $this->table, self::SECTION_DELETED, $section_id);
    }
}

