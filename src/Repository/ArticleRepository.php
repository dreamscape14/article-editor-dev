<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Repository;


use Dreamscape\Database\QueryFilter;
use Dreamscape\Model\Article;
use Dreamscape\Repository\Filters\ActiveArticleFilter;
use Dreamscape\Repository\Filters\ArticleIdArticleFilter;
use Dreamscape\Repository\Filters\MissingTagsArticleFilter;
use Dreamscape\Repository\Filters\RecentlyInsertedArticleFilter;
use Dreamscape\Repository\Filters\RecentlyUpdatedArticleFilter;
use PDO;

final class ArticleRepository extends Repository
{
    use WithArticleStatuses;

    public static $FILTERS = ['section_id', 'article_id', 'status_id', 'missing_tags'];

    protected $filterClassPostfix = 'ArticleFilter';

    protected $defaultQuery = 'queryShort';

    protected function globalFilters()
    {
        return [
            new ActiveArticleFilter($this->articleStatusId('delete')),
        ];
    }

    public function queryShort()
    {
        $query = '
            SELECT article.article_id, article.article_url, article.article_title, 
                   article.date_scanned, article.date_published,
                   article.date_updated, article.status_id, 
                   generic_status.status_name as status,
                   generic_status.status_color
            FROM article article
                LEFT JOIN generic_status generic_status ON article.status_id = generic_status.status_id
                LEFT JOIN article_sections article_sections USING (section_id)
        ';
        return $query;
    }
    
    public function queryFull()
    {
        return '
            SELECT article.article_id, article.file_id, article.article_url, article.article_title, article.article_description,
                   article.article_tags, article.section_id, article.article_content, article.weight, article.status_id, 
                   article.date_scanned, gs.status_name,
                   article.date_published, article.date_updated, article.doc_type, 
                   asec.section_title
            FROM article article
                LEFT JOIN generic_status gs USING(status_id)
                LEFT JOIN article_sections asec USING(section_id)
                LEFT JOIN tag_to_article tta  USING(article_id)
        ';
    }

    public function recenltyInserted($limit = 0)
    {
        return $this->fetchAll($this->queryShort(), [
            new RecentlyInsertedArticleFilter(),
        ], $limit);
    }

    public function recenltyUpdated($limit = 0)
    {
        return $this->fetchAll($this->queryShort(), [
            new RecentlyUpdatedArticleFilter(),
        ], $limit);
    }

    public function missingTags($page, $per_page = 15)
    {
        return $this->filterBy($this->queryFull(), [
            new MissingTagsArticleFilter()
        ], $page, $per_page);
    }

    public function articleId($article_id)
    {
        return  $this->fetch($this->queryFull(), [
            new ArticleIdArticleFilter($article_id)
        ]);
    }

    public function excludedResellers($article_id)
    {
        $query = '	SELECT distinct aer.reseller_id
					FROM sYra_help.article_exclude_reseller aer
					WHERE article_id = :article_id';
        return $this->db()->query($query, [':article_id' => $article_id])->fetchAll(PDO::FETCH_COLUMN);
    }

    public function excludedLocales($article_id)
    {
        $query = '	SELECT distinct al.exclude_locale
					FROM sYra_help.article_locale al
					WHERE article_id = :article_id';
        return $this->db()->query($query, [':article_id' => $article_id])->fetchAll(PDO::FETCH_COLUMN);
    }

    public function findOrNew($article_id)
    {
        $data = [];
        
        if ((int) $article_id !== 0) {
            $this->disableGlobalFilters();
            $data = $this->articleId($article_id);
            $data['excluded_resellers'] = $this->excludedResellers($article_id);
            $data['excluded_locales'] = $this->excludedLocales($article_id);
            $this->enableGlobalFilters();
        }

        return Article::create($data);
    }

}

