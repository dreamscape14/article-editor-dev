<?php


namespace Dreamscape\Repository\Schutz;


use Dreamscape\Repository\Repository;

class CrmsUsersRepository extends Repository
{
    public function userDetails($user_id)
    {
        $user_id = $this->imposeIntId($user_id);
        $query = '	SELECT username, password, full_name
				  	FROM sYra.crms_users
				  	WHERE user_id = :user_id';

        return $this->db()->query($query, [':user_id' => $user_id])->fetch();
    }
}

