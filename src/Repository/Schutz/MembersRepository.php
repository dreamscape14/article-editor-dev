<?php


namespace Dreamscape\Repository\Schutz;


use Dreamscape\Repository\Repository;

class MembersRepository extends Repository
{
    public function memberDetails($member_id)
    {
        $member_id = $this->imposeIntId($member_id);
        $query = '	SELECT member_id, reseller_id
					FROM sYra.members
					WHERE member_id = :member_id';

        return $this->db()->query($query, [':member_id' => $member_id])->fetch();
    }
}

