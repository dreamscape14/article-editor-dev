<?php

/**
 * @PhpVersionCheck:5.4
 */

namespace Dreamscape\Foundation;


use cart\controller\Exception;
use Dreamscape\Exceptions\AccessNotAllowed;
use Dreamscape\Repository\Schutz\CrmsUsersRepository;
use Dreamscape\Repository\Schutz\MembersRepository;

final class ACL
{
    public static $ARTICLE_TOOL_ROLES = [
        'ARTICLE_TOOL_EDITOR_ROLE',
        'ARTICLE_TOOL_PUBLISHER_ROLE',
        'ARTICLE_TOOL_VIEW',
    ];

    public static $MODERATOR_IDS = [
        \environment::PRODUCTION => ['member_id' => '0', 'crms_user_id' => '0',],
        \environment::DEVELOPMENT => ['member_id' => '7165901', 'crms_user_id' => '3187',],
    ];

    public static function roles()
    {
        $result = [];
        foreach (self::$ARTICLE_TOOL_ROLES as $role) {
            $result[$role] = \crms_user::check_current_permissions($role);
        }
        return $result;
    }

    public static function ensureAny(array $permissions)
    {
        try {
            $any = array_map(static function ($role) {
                return \crms_user::check_current_permissions($role);
            }, $permissions);
        } catch (\Exception $e) {
            $any = [];
        }
        return count(array_filter($any)) > 0;
    }

    public static function ensurePermissions(array $permissions)
    {
        foreach ($permissions as $role) {
            if (! \crms_user::check_current_permissions($role) ) {
                return false;
            }
        }
        return true;
    }

    public static function generateCrmsLoginToken()
    {
        $member_id = self::$MODERATOR_IDS[\environment::get()]['member_id'];
        $user_id = self::$MODERATOR_IDS[\environment::get()]['crms_user_id'];

        $member_details = (new MembersRepository())->memberDetails($member_id);
        $user_details = (new CrmsUsersRepository())->userDetails($user_id);

        $token_data = [
            'member_id' => $member_details['member_id'],
            'reseller_id' => $member_details['reseller_id'],
            'crms' => array(
                'username' => $user_details['username'],
                'password' => $user_details['password'],
            ),
            'expiry' => time() + 1800,
        ];

        $cryptography = new \cryptography();
        $cryptography->set_iv($cryptography->generate_iv());
        $cryptography->set_key(LOGIN_IN_AS_ENCRYPTION_KEY);

        $login_member_token = urlencode($cryptography->encrypt(serialize($token_data), true));

        return $login_member_token;
    }

    public static function current_user_id()
    {
        return \crms_user::get_current_user()->user_id;
    }
}

