<?php

namespace Dreamscape\Http;

final class JsonResponse
{
    public $code;
    public $message;
    public $environment;
    public $data;

    private function __construct($code = 0, $message = 'ok')
    {
        $this->code = $code;
        $this->message = $message;
        $this->environment = \environment::get();
        $this->data = [];
    }

    public function setData($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function error($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public static function ok($code = 0, $message = 'ok')
    {
        return new static($code, $message);
    }

}