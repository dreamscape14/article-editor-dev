<?php

$files = glob(__DIR__ . '/UseCase/*.php');
if ($files === false) {
    throw new RuntimeException('Failed to autoload use cases library');
}

foreach ($files as $file) {
    require_once $file;
}



