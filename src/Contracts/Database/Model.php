<?php


namespace Dreamscape\Contracts\Database;


interface Model
{
    public static function create(array $data);
}

